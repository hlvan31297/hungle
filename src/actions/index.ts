// export action creators
import * as loginActions from '@actions/login.actions';
import * as userCheckActions from '@actions/user-check.actions';

export const ActionCreators = Object.assign(
    {},
    loginActions,
    userCheckActions,
);
