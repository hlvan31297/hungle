/*
 * Reducer actions related with navigation
 */
import AppStackScreen from '../navigation/navigation.navigator';
import { Screen } from '../navigation/index';
export function bottomTabs(params?: any) {
    AppStackScreen.navigate(Screen.bottomTabs, params);
}

