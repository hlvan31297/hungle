/*
 * Reducer actions related with login
 */
import * as UserCheckTypes from '@constants/user-check.constant';
import * as LoadingTypes from '@constants/loading.constant';
import { ILoginResponse } from '@models/login.model';

export function requestUserCheck() {
    return {
        type: UserCheckTypes.USER_CHECK_REQUEST,
    };
}

export function userCheckFailed() {
    return {
        type: UserCheckTypes.USER_CHECK_FAIL,
    };
}

export function onUserCheckResponse(response: ILoginResponse) {
    return {
        type: UserCheckTypes.USER_CHECK_RESPONSE,
        response,
    };
}

export function enableLoader() {
    return {
        type: LoadingTypes.LOGIN_ENABLE_LOADER,
    };
}

export function disableLoader() {
    return {
        type: LoadingTypes.LOGIN_DISABLE_LOADER,
    };
}

