/*
 * Reducer actions related with login
 */
import * as LoginTypes from '@constants/login.constant';
import * as LoadingTypes from '@constants/loading.constant';
import { ILoginResponse } from '@models/login.model';

export function requestLogin(email: string, password: string) {
    return {
        type: LoginTypes.LOGIN_REQUEST,
        email,
        password,
    };
}

export function loginFailed() {
    return {
        type: LoginTypes.LOGIN_FAILED,
    };
}

export function onLoginResponse(response: ILoginResponse) {
    return {
        type: LoginTypes.LOGIN_RESPONSE,
        response,
    };
}

export function enableLoader() {
    return {
        type: LoadingTypes.LOGIN_ENABLE_LOADER,
    };
}

export function disableLoader() {
    return {
        type: LoadingTypes.LOGIN_DISABLE_LOADER,
    };
}

export function logOut() {
    return {
        type: LoginTypes.LOG_OUT,
    };
}
