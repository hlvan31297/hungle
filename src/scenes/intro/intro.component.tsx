import React from 'react';
import ContentView from '../../layouts/intro';

export const IntroScreen = ({ navigation }): React.ReactElement => (
  <ContentView navigation={navigation}/>
);
