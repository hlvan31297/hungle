import React from 'react';
import ContentView from '@layouts/bottomTabs';

export const bottomTabs = ({ navigation }): React.ReactElement => (
  <ContentView navigation={navigation}/>
);
