import React from 'react';
import ContentView from '../../layouts/Information';

export const InformationScreen = ({ navigation }): React.ReactElement => (
  <ContentView navigation={navigation}/>
);