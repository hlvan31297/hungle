import React from 'react';
import ContentView from '../../layouts/browse';

export const browse = ({ navigation }): React.ReactElement => (
  <ContentView navigation={navigation}/>
  
);
