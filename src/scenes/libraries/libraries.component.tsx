import React, { useRef, useState } from 'react';
import {
  Text,
  Dimensions,
  Image,
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  Platform  
} from 'react-native';

import { IMAGE } from '../../components/image';

  const { width: screenWidth, height: screenHeight } = Dimensions.get('window');

  const lineBreak = '\n';

  const slides = [
    {
      title: `stairは、高学歴かつアスリート${lineBreak}学生に限定した新しい${lineBreak}就活プラットフォームです。`,
      image: IMAGE.INTRO_1,
      button: '次へ',
      buttonSize : true, 
      id: 1,
    },
    {
      title: `SNSを通じて、1年生から${lineBreak}企業にアピールすることが${lineBreak}でき、企業からもオファーが${lineBreak}届きます。`,
      image: IMAGE.INTRO_2,
      button: '次へ',
      id: 2,
    },
    {
      title: `学生同士も友達として${lineBreak}つながることができます。${lineBreak}それが、未来の人脈形成に${lineBreak}大きく役立つかもしれません。`,
      image: IMAGE.INTRO_3,
      button: '次へ',
      id: 3,
    },
    {
      title: `熱狂する力を社会へ！${lineBreak}高学歴アスリート学生を求める${lineBreak}企業とエンゲージメントを深め${lineBreak}あなたにとって最適な${lineBreak}就職先を見つけましょう！`,
      image: IMAGE.INTRO_4,
      button: 'さあ、はじめましょう',
      id: 4,
    },
  ];


export const LibrariesScreen = () => {
  const scrollRef = useRef<ScrollView>(null); 
  const [currentSlide, setCurrentSlide] = useState(0);
  const onchange = (nativeEvent) => {
    if(nativeEvent){
      const slide = Math.ceil(nativeEvent.contentOffset.x/nativeEvent.layoutMeasurement.width);
      if(slide != currentSlide ){
        setCurrentSlide(slide);
      }
    }
  };

  const handleScrollEnd = (e: any) => {
    if (!e) {
      return
    }
    const { nativeEvent } = e
    if (nativeEvent && nativeEvent.contentOffset) {
      let currentSlide = 1
      if (nativeEvent.contentOffset.x === 0) {
        setCurrentSlide(currentSlide)
      } else {
        const approxCurrentSlide = nativeEvent.contentOffset.x / screenWidth
        currentSlide = Math.ceil(parseInt(approxCurrentSlide.toFixed(2)) + 1)
        setCurrentSlide(currentSlide)
      }
    }
  }

  const onNextButtonPress = () => {
    const scrollPoint = (currentSlide + 1) * screenWidth
    scrollRef?.current.scrollTo({
      x: scrollPoint,
      y: 0,
      animated: true
    })
    if (Platform.OS === 'android') {
      handleScrollEnd({ nativeEvent: { contentOffset: { y: 0, x: scrollPoint } } })
    }
  }

  return (
    <View style={styles.container}>
      <ScrollView
        ref={scrollRef}
        onScroll={({nativeEvent}) => onchange(nativeEvent)}
        horizontal
        nestedScrollEnabled={true}
        decelerationRate={0.1}
        showsHorizontalScrollIndicator={false}
        pagingEnabled 
        scrollEventThrottle={16}
      >
        {
          slides.map((item, _) =>
            <View key={item.id}>
              <Image
                source={item.image}
                style={styles.wsap}
              />
              <View style={styles.child}>
                <Text style={styles.textTitleStyle}>{item.title}</Text>
              </View>
            
              <View style={styles.viewbotton}>
                
                  <TouchableOpacity
                  style={styles.signInButton}
                  onPress={() => onNextButtonPress()}
                  >
                    <Text style={styles.textButtonStyle}>
                      {item.button}
                    </Text>
                  </TouchableOpacity>
                
              </View>
            </View>
          )
        }
      </ScrollView>
      <View style={styles.wrapDot}>
        {
          slides.map((item, index)=>
            <Text
              key={item.id}
              style={currentSlide == index ? styles.dotActive : styles.dot}
            >
              ●
            </Text>
          )
        }
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "black",
  },


  image: {
    ...StyleSheet.absoluteFillObject,
    width: undefined,
    height: undefined,
  },
  wsap :{
    width: screenWidth,
    height: screenHeight,
  },
  wrapDot:{
    position : 'absolute',
    bottom : 0,
    flexDirection:'row',
    alignSelf:'center',
  },
  dotActive:{
    margin : 3,
    color :'#fff'
  },
  dot:{
    margin:3,
    color : '#888'
  },
  texttitle:{
    marginTop: 100,
    flex: 1,
    alignItems: 'center', 
    marginHorizontal: 5
  },
  textTitleStyle: {
    fontSize: screenWidth < 370 ? 22 : 24, 
    color: '#fff', 
    textAlign: 'center',
    fontWeight: '400',
    position: 'relative',
  },

  child: {
    position: 'absolute',
    marginTop: 100,
    flex: 1,
    alignItems: 'center', 
    marginHorizontal: 5,
    width:'100%',
    display: 'flex',
    justifyContent: 'center'
  },
  textButtonStyle: {
    fontSize: screenWidth < 370 ? 15 : 17, 
    color: '#fff', 
    textAlign: 'center', 
    paddingVertical: 15, 
    paddingHorizontal: 60,
    fontWeight: '400',
    opacity: 1,
    
  },
   buton :{
    //  width: 200
   },
   signInButton:{
    backgroundColor: '#2680EB', 
    alignSelf:'center', 
    borderRadius: 20,
    alignItems: 'center',
   },
   viewbotton :{
    alignItems: 'center',
    position: 'absolute', 
    display: 'flex', width: '100%', 
    justifyContent: 'center', 
    left: 0, 
    bottom: 100
   }
});
