import React from 'react';
import ContentView from '../../layouts/myPage';

export const myPage = ({ navigation }): React.ReactElement => (
  <ContentView navigation={navigation}/>
);