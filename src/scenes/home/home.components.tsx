import React from 'react';
import ContentView from '@layouts/home';

export const home = ({ navigation }): React.ReactElement => (
  <ContentView navigation={navigation}/>
);
