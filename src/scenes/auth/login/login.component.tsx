import React from 'react';
import ContentView from '@layouts/auth/login/index';

export const login = ({ navigation }): React.ReactElement => (
  <ContentView navigation={navigation}/>
);