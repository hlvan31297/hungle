import React from 'react';
import ContentView from '@layouts/auth/registration/survey';

export const survey = ({ navigation }): React.ReactElement => (
  <ContentView navigation={navigation}/>
);