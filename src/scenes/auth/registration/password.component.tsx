import React from 'react';
import ContentView from '@layouts/auth/registration/password';

export const password = ({ navigation }): React.ReactElement => (
  <ContentView navigation={navigation}/>
);