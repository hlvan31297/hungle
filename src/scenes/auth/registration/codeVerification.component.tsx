import React from 'react';
import ContentView from '@layouts/auth/registration/codeVerification';

export const codeVerification = ({ navigation }): React.ReactElement => (
  <ContentView navigation={navigation}/>
);