import React from 'react';
import ContentView from '@layouts/auth/registration/email';

export const email = ({ navigation }): React.ReactElement => (
  <ContentView navigation={navigation}/>
);