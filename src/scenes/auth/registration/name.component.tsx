import React from 'react';
import ContentView from '@layouts/auth/registration/name';

export const name = ({ navigation }): React.ReactElement => (
  <ContentView navigation={navigation}/>
);