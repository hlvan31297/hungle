import React from 'react';
import {
    StyleSheet,
    Platform,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
const SignUpHeaderComponent = (props) => {
    const {
        title,
        onPress,
        onShowHeaderLeft,
    } = props;
    return (
        <View style={styles.headerStyle}>
            <View style={styles.headerLeftStyle}>
                {
                    onShowHeaderLeft ? null :
                    <TouchableOpacity
                        onPress={onPress}
                        style={{marginLeft: 16}}
                    >
                        <MaterialIcons name='chevron-left' size={25} color='#2680EB' />
                    </TouchableOpacity>
                }
            </View>
            <View style={styles.headerCenterStyle}>
                <Text style={styles.titleStyle}>
                    {title}
                </Text>
            </View>
            <View style={styles.headerRightStyle}>

            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    headerStyle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 60,
        flexDirection: 'row',
    },
    headerLeftStyle: {
        flex: 1,
    },
    headerCenterStyle: {
        flex: 8,
        alignItems: 'center',
    },
    headerRightStyle: {
        flex: 1,
    },
    titleStyle: {
        color: '#263238',
        fontSize: 22,
        fontWeight: 'bold',
      },
});

export default SignUpHeaderComponent;
