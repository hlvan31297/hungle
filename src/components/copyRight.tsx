import React, {useState} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Platform,
} from 'react-native';
import {IMAGE} from '../components/image';

const CopyRight = (props: any) => {
    const {
        isShowLogo,
    } = props;

    return (
        <View style={{width: '100%', alignItems: 'center'}}>
            {isShowLogo ? <Image source={IMAGE.STAIR_LOGO_BLUE} style={{width: 96, height: 48}}/> : null}
            <Text style={{color: '#263238', fontSize: 11, fontWeight: '500'}}>
                ©️2020 stair.Inc
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({});

export {CopyRight};
