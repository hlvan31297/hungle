import { Image } from 'react-native';
const IMAGE = {
    ICON_PROFILE: require('../assets/images/iconProfile.png'),
    NO_PICTURE: require('../assets/images/unnamed.jpg'),
    BACKGROUND_PROFILE: require('../assets/images/noBackground.png'),
    PDF_FILE: require('../assets/images/pdfFile.png'),
    INTRO_1: require('../assets/images/intro-image-01.jpeg'),
    INTRO_2: require('../assets/images/intro-image-02.jpg'),
    INTRO_3: require('../assets/images/intro-image-03.jpg'),
    INTRO_4: require('../assets/images/intro-image-04.jpg'),
    INTRO_5: require('../assets/images/intro-image-05.json'),
    INTRO_6: require('../assets/images/intro-image-06.json'),
    INTRO_7: require('../assets/images/intro-image-07.json'),
    INTRO_8: require('../assets/images/image-message-2.jpg'),
    SETTING_SCOPE_SUB_1: Image.resolveAssetSource(require('../assets/images/setting-image-1.png')).uri,
    SETTING_SCOPE_SUB_2: Image.resolveAssetSource(require('../assets/images/setting-image-2.png')).uri,
    FIRST_GO_TO_MY_PAGE: require('../assets/images/first-go-to-my-page.json'),
    BACKGROUND_INTRO: require('../assets/images/background-intro.jpg'),
    ICON_STAIR_1: require('../assets/icons/icon-stair-1.png'),
    ICON_STAIR_2: require('../assets/icons/icon-stair-2.png'),
    ICON_STAIR_3: require('../assets/icons/icon-stair-3.png'),
    STAIR_LOGO_BLUE: require('../assets/icons/stair_logo_blue.png'),
    REGISTER_IMAGE_1: require('../assets/images/register-image-1.json'),
    RESEARCH_CONTENT : require('../assets/images/24827-learn.json'),
    SETTING_SCOPE : require('../assets/images/13009-settings.json'),
    UNSUBSCRIBED : require('../assets/images/32338-attention.json'),
    POST_DETAIL_DELETE : require('../assets/images/2942-delete-bubble.json'),
    REPORT_MODAL_CHECK : require('../assets/images/32874-blue-check.json'),
    MY_PAGE_PROFILE_MISSING: require('../assets/images/my-page-profile-missing.json'),
    REFERENCE_GUIDE: require('../assets/images/refer-guide.json'),
    REGIST_WORK: require('../assets/images/regist-work.json'),
    HELLO_DIALOG: require('../assets/images/hello-dialog.json'),
      UPLOAD_SNS: require('../assets/images/upload-sns.json'),
    CHAT_GUIDE: require('../assets/images/chat-guide.json'),
    SEARCH_PAGE_USER: require('../assets/images/35132-people-with-mobile-phones.json'),
    SEARCH_PAGE_RATE: require('../assets/images/21202-first-place.json'),
    SEARCH_PAGE_REST: require('../assets/images/34789-loader.json'),
    MEDIA_GUIDE: require('../assets/images/media-guide.json'),
    MEDIA_INTERN_REGISTER: require('../assets/images/media-intern-register.json'),
    MEDIA_INTERN_REGISTER_1: require('../assets/images/media-intern-register-1.json'),
    MEDIA_SEARCH_GUIDE: require('../assets/images/media-search-guide.png'),
    CHECK_MARK: require('../assets/images/checkmark.gif'),
    ICON_PROFILE_URI: Image.resolveAssetSource(require('../assets/images/iconProfile.png')).uri,
    NO_PICTURE_URI: Image.resolveAssetSource(require('../assets/images/unnamed.jpg')).uri,
    NO_MEETING_IMAGE_URI: Image.resolveAssetSource(require('../assets/images/meeting.png')).uri,
    NO_COMPANY_PROFILE_URI: Image.resolveAssetSource(require('../assets/images/company_icon3.png')).uri,
    BACKGROUND_PROFILE_URI: Image.resolveAssetSource(require('../assets/images/noBackground.png')).uri,
    BLACK_BACKGROUND_URI: Image.resolveAssetSource(
        require('../assets/images/black-background.jpg')).uri,
    REGISTER_IMAGE_2: require('../assets/images/image-json-1.json'),
};

export {IMAGE};
