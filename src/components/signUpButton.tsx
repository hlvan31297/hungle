import React from 'react';
import {
    StyleSheet,
    Platform,
    View,
    Text,
    ActivityIndicator,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
const SignUpButton = (props) => {

    const {
        title,
        onPress,
        loading,
        style,
        disable,
    } = props;

    const onNextPress = () => {
        onPress();
    };

    if (loading) {
        return (
            <View style={styles.loadingStyle}>
            <ActivityIndicator
                size='large'
                color='#2680EB'
                style={style === 1 ? {} : {}}
            />
            </View>
        );
    } else {
        return (
        <TouchableOpacity
            style={style === 1 ? styles.buttonStyle :
                [styles.buttonStyle2, disable ? styles.buttonValidateStyle1 : styles.buttonValidateStyle2]}
            onPress={() => onNextPress()}
            disabled={disable}
            >
                <Text style={style === 1 ? styles.textButton : [styles.textButton2]}>
                    {title}{disable}
                </Text>
            </TouchableOpacity>
        );
    }
};

const styles = StyleSheet.create({
    loadingStyle: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonStyle: {
        height: 50,
        borderWidth: 1,
        borderColor: '#2680EB',
        backgroundColor: '#fff',
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
    },
    textButton: {
        fontSize: 16,
        color: '#2680EB',
        fontWeight: '700',
    },
    buttonStyle2: {
        height: 50,
        borderWidth: 1,
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textButton2: {
        fontSize: 16,
        color: '#fff',
        fontWeight: '700',
    },
    buttonValidateStyle1: {
        borderColor: '#C0C0C0',
        backgroundColor: '#C0C0C0',
    },
    buttonValidateStyle2: {
        borderColor: '#2680EB',
        backgroundColor: '#2680EB',
    },
});

export default SignUpButton;
