import {
    StyleSheet, Dimensions,
} from 'react-native';

const widthS = Dimensions.get('window').width;
const heightS = Dimensions.get('window').height;

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    titleStyle: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#3A3A3A',
    },
    imageStyle: {
        width: '100%',
        height: '100%',
    },
    imageStyle1: {
        width: (widthS / 3),
        height: (widthS / 3),
    },
    imageForm: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    mediaForm: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingHorizontal: 8,
    },
    imageForm_2: {
        width: widthS,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    selectedStyle: {
        borderColor: '#d9d9d9',
        borderRadius: 4,
        borderWidth: 1,
        backgroundColor: '#fff',
        maxHeight: 200,
        marginVertical: 8,
    },
    buttonStyle: {
        backgroundColor: '#2680EB',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: 50,
        borderRadius: 3,
    },
    textButtonStyle: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 14,
    },
    buttonStyle2: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: 50,
        borderRadius: 3,
        borderColor: '#D9D9D9',
        borderWidth: 1,
    },
    textButtonStyle2: {
        color: '#263238',
        fontWeight: 'bold',
        fontSize: 14,
    },
    buttonStyle3: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: 50,
        borderRadius: 3,
        borderColor: '#2680EB',
        borderWidth: 2,
    },
    textButtonStyle3: {
        color: '#2680EB',
        fontWeight: 'bold',
        fontSize: 14,
    },
    errMessage: {
        color: 'red',
        fontSize: 11,
    },
    modalView: {
        backgroundColor: '#fff',
        height: widthS,
        width: widthS - 32,
        borderRadius: 5,
    },
    buttonModalStyle: {
        marginTop: 10,
        marginRight: 10,
        alignItems: 'flex-end',
    },
    iconModalStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
    },
    textViewModalStyle1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 20,
    },
    textModalStyle1: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#263238',
        textAlign: 'center',
    },
    viewButtonStyle1: {
        height: 48,
        borderRadius: 4,
        marginHorizontal: 20,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2680EB',
    },
    viewButtonStyle2: {
        height: 48,
        borderRadius: 4,
        borderWidth: 2,
        borderColor: '#2680EB',
        marginHorizontal: 20,
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textButtonModalStyle: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    textModalStyle2: {
        fontSize: 14,
        color: '#263238',
        textAlign: 'center',
        marginTop: 30,
    },
    textLeftModalStyle: {
        fontSize: 14,
        color: '#263238',
        textAlign: 'left',
    },
    rightButton: {
        color: '#2680EB',
        fontSize: 14,
        fontWeight: 'bold',
    },
    modalImageStyle: {
        width: 200,
        height: 200,
    },
    modalView2: {
        backgroundColor: '#fff',
        height: heightS / 1.5,
        width: widthS - 32,
        borderRadius: 5,
    },
    selectStyle: {
        borderColor: '#D9D9D9',
        borderRadius: 4,
        borderWidth: 1,
        backgroundColor: '#fff',
        maxHeight: 200,
        marginVertical: 8,
    },
    introSlideStyle: {
        marginHorizontal: 4,
        width: 8,
        height: 8,
        borderRadius: 8 / 2,
        borderWidth: 1,
        borderColor: '#fff',
    },
    introSlideActiveStyle: {
        marginHorizontal: 4,
        width: 8,
        height: 8,
        borderRadius: 8 / 2,
        backgroundColor: '#fff',
    },
});

export { Styles };
