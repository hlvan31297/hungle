import { ApiClient } from '@services/api.service';
import { JSObject } from '@models/axios.model';

const baseURL = 'https://dev.stair-sp.online/api';
const apiClient = new ApiClient(baseURL);

const USER_CHECK_PATH = '/me';

export class UserCheckApi {
    static get(params: JSObject): Promise<{}> {
        return apiClient.get(USER_CHECK_PATH, params);
    }
}
