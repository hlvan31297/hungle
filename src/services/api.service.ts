import axios, { AxiosError, AxiosInstance, AxiosRequestConfig } from 'axios';

import { AxiosResponse } from '@models/axios.model';
import configureStore from '@src/store';
import AsyncStorage from '@react-native-async-storage/async-storage';

// const { store, persistor } = configureStore();
export class ApiClient {
    axiosInstance: AxiosInstance;

    constructor(baseURL = '') {
        const sourceRequest = {};
        this.axiosInstance = axios.create({
            baseURL: baseURL,
            headers: {
                'Cache-Control': 'no-cache',
                'Content-Type': 'application/json',
            },
            timeout: 30000,
        });

        this.axiosInstance.interceptors.request.use(
            async (config: AxiosRequestConfig) => {
                return config;
            },
            (err: AxiosError) => {
                return Promise.reject(err);
            },
        );

        this.axiosInstance.interceptors.request.use(
            request => {
              // If the application exists cancel
              if (sourceRequest[request.url]) {
                sourceRequest[request.url].cancel('Automatic cancellation');
              }

              // Store or update application token
              const axiosSource = axios.CancelToken.source();
              sourceRequest[request.url] = { cancel: axiosSource.cancel };
              request.cancelToken = axiosSource.token;

              return request;
            },
            error => {
              return Promise.reject(error);
            },
        );

        // Response interceptor for API calls
        this.axiosInstance.interceptors.response.use((response) => {
            return response;
        }, async function (error) {
            const originalRequest = error.config;
            if (error.response.status === 401 && !originalRequest._retry) {
                originalRequest._retry = true;
                const access_token = await this.refreshAccessToken();
                this.axiosInstance.defaults.headers.common.Authorization = 'Bearer ' + access_token;
                return this.axiosInstance(originalRequest);
            }
            return Promise.reject(error);
        });
    }

    async get<T = object>(path: string, params: object = {}): Promise<AxiosResponse<T>> {
        try {
            this.setAuthorizationHeader(await this.getAccessToken());
            const result = await this.axiosInstance.get(path, { params });
            return this.createSuccessPromise<T>(result.data);
        } catch (e) {
            return this.createFailurePromise<T>(e);
        }
    }

    async post<T = object>(path: string, params: object = {}): Promise<AxiosResponse<T>> {
        try {
            this.setAuthorizationHeader(await this.getAccessToken());
            const result = await this.axiosInstance.post<T>(path, params);
            return this.createSuccessPromise<T>(result.data);
        } catch (e) {
            return this.createFailurePromise<T>(e);
        }
    }

    async put<T = object>(path: string, params: object = {}): Promise<AxiosResponse<T>> {
        try {
            const result = await this.axiosInstance.put<T>(path, params);
            return this.createSuccessPromise<T>(result.data);
        } catch (e) {
            return this.createFailurePromise<T>(e);
        }
    }

    async delete<T = object>(path: string): Promise<AxiosResponse<T>> {
        try {
            const result = await this.axiosInstance.delete(path);
            return this.createSuccessPromise<T>(result.data);
        } catch (e) {
            return this.createFailurePromise<T>(e);
        }
    }

    async patch<T = object>(path: string, params: object = {}): Promise<AxiosResponse<T>> {
        try {
            const result = await this.axiosInstance.patch<T>(path, params);
            return this.createSuccessPromise<T>(result.data);
        } catch (e) {
            return this.createFailurePromise<T>(e);
        }
    }

    private getAccessToken = (): Promise<string> => AsyncStorage.getItem('access_token');

    private setAuthorizationHeader = (access_token: string) => {
        this.axiosInstance.defaults.headers.common.Authorization = 'Bearer ' + JSON.parse(access_token);
    };

    private refreshAccessToken = () => '';

    private createSuccessPromise<T>(data: T): Promise<AxiosResponse<T>> {
        return Promise.resolve<AxiosResponse<T>>({ data, isSuccess: true });
    }

    private createFailurePromise<T>(error: AxiosError): Promise<AxiosResponse<T>> {
        return Promise.resolve<AxiosResponse<T>>({ error, isSuccess: false });
    }
}
