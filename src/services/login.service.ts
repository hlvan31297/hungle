import { ApiClient } from '@services/api.service';
import { JSObject } from '@models/axios.model';

const baseURL = 'https://dev.stair-sp.online/api';
const apiClient = new ApiClient(baseURL);

const LOGIN_PATH = '/auth/login';

export class LoginApi {
    static post(params: JSObject): Promise<{}> {
        return apiClient.post(LOGIN_PATH, params);
    }
}
