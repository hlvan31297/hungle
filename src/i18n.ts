import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { config } from '@config/index';
import enTranslation from '@assets/locales/en/translation.json';
import jpTranslation from '@assets/locales/jp/translation.json';
import viTranslation from '@assets/locales/vi/translation.json';

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources: {
      en: {
        trans: enTranslation,
      },
      vi: {
        trans: viTranslation,
      },
      jp:{
        trans: jpTranslation,
      }
    },
    lng: config.defaultLang,

    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
