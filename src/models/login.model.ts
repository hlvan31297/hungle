export interface ILoginResponse {
  data: ILoginResponseData;
  isSuccess: boolean;
}

export interface ILoginRequestState {
  type: String;
  email: string;
  password: string;
  role: string;
}

export interface ILoginResponseState {
  type: String;
  data: ILoginResponseData;
}

export interface ILoginState {
  isLoggedIn: boolean;
  email: string;
  password: string;
  access_token: string;
  email_verified: boolean;
  token_cognito: ICognitoData;
}

export interface ILoginResponseData {
  access_token: string;
  token_cognito: ICognitoData;
  email_verified: boolean;
}

export interface ICognitoData {
  AccessToken: string;
  RefreshToken: string;
  IdToken: string;
}

export interface IApi {
  fetching: boolean;
  error?: string;
}

export interface IAuthType extends IApi {
  token: string;
  user: IUser;
}

export interface IUser {
  name: string;
  gender: string;
  id: string;
}
