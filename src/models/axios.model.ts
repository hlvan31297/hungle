import { AxiosError } from 'axios';

export interface AxiosResponse<T> {
    data?: T;
    error?: AxiosError;
    isSuccess: boolean;
}

export interface JSObject { [p: string]: any; }
