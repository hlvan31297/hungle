export interface IUserCheckState {
    data: any;
}

export interface IUserCheckRequestState {
    data: any;
}

export interface IUserCheckResponseState {
    response: {
        data: {
            data: any;
        },
    };
}
