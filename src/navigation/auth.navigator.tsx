import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { AuthScreen } from '@scenes/auth/auth.component';
import { IntroScreen} from '@scenes/intro/intro.component';
import { survey } from '@scenes/auth/registration/survey.component';
import { login } from '@scenes/auth/login/login.component';
import { name } from '@scenes/auth/registration/name.component';
import { email } from '@scenes/auth/registration/email.component';
import { password } from '@scenes/auth/registration/password.component';
import { codeVerification } from '@scenes/auth/registration/codeVerification.component';
const TopTab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

const AuthMenuNavigator = (): React.ReactElement => (
  <TopTab.Navigator tabBar={(props) => <AuthScreen {...props}/>}>
  </TopTab.Navigator>
);

export const AuthNavigator = (): React.ReactElement => (
  <Stack.Navigator headerMode='none'>
    <Stack.Screen name = 'IntroScreen' component = {IntroScreen}/>
    <Stack.Screen name = 'survey' component = {survey}/>
    <Stack.Screen name = 'name' component = {name}/>
    <Stack.Screen name = 'email' component = {email}/>
    <Stack.Screen name = 'password' component = {password}/>
    <Stack.Screen name = 'codeVerification' component = {codeVerification}/>
    <Stack.Screen name = 'login' component = {login}/>
  </Stack.Navigator>
);
