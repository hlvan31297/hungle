import * as React from 'react';
import { NavigationContainerRef } from '@react-navigation/native';

export const navigationRef: React.RefObject<NavigationContainerRef> = React.createRef();

const navigate = (name: string, params: any) => {
    if (navigationRef.current) {
        return navigationRef.current.navigate(name, params);
    } else {
        console.error('!!!!not mounted yet!!!!!!!');
    }
};

const reset = (name: string, params: any) => {
    if (navigationRef.current) {
        return navigationRef.current.reset({
            index: 0,
            routes: [
                {
                    name,
                    params,
                },
            ],
        });
    } else {
        console.error('!!!!not mounted yet!!!!!!!');
    }
};

export default {
    navigate,
    reset,
};
