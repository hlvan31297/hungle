import React from 'react';
import { LogBox, StyleSheet, View, Text } from 'react-native';
import {
  BottomTabNavigationOptions,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { bottomTabs } from '@scenes/bottomTabs/bottomTabs.components';

import { AuthNavigator } from './auth.navigator';
import Icon from 'react-native-vector-icons/Foundation';

const BottomTab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
/*
 * When dev is true in .expo/settings.json (started via `start:dev`),
 * open Components tab as default.
 */
const initialTabRoute: string = __DEV__ ? 'Components' : 'Layouts';

const ROOT_ROUTES: string[] = ['Home', 'Layouts', 'Components', 'Themes'];

const TabBarVisibilityOptions = ({ route }): BottomTabNavigationOptions => {
  const isNestedRoute: boolean = route.state?.index > 0;
  const isRootRoute: boolean = ROOT_ROUTES.includes(route.name);

  return { tabBarVisible: isRootRoute && !isNestedRoute };
};


export const AppStackScreen = (): React.ReactElement => (
  <Stack.Navigator headerMode='none' >
    <Stack.Screen name='auth' component={AuthNavigator}/>
    <Stack.Screen name = 'bottomTabs' component = {bottomTabs}/>
  </Stack.Navigator>
);


LogBox.ignoreLogs(['Accessing the \'state\'']);
