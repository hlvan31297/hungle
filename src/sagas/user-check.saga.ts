// /* Redux saga class
//  * logins the user into the app
//  * requires email and password.
//  * un - email
//  * pwd - password
//  */
// import { put, call, select } from 'redux-saga/effects';
// // import { delay } from 'redux-saga';

// import { Alert } from 'react-native';

// import * as userCheckActions from '@actions/user-check.actions';
// import { UserCheckApi } from '@services/user-check.service';
// import * as navigationActions from '@actions/navigation.actions';

// export const getEmail = (state: any) => state.loginReducer.email;
// export const getPassword = (state: any) => state.loginReducer.password;

// // Our worker Saga that logins the user
// export default function* userCheckAsync() {
//     yield put(userCheckActions.enableLoader());

//     // How to call api
//     const response = yield call(UserCheckApi.get, {});
//     // Mock response
//     // const response = { success: true, data: { id: 1 }, message: 'Success' };
//     // console.log(response.data);

//     if (response.isSuccess) {
//         yield put(userCheckActions.onUserCheckResponse(response));
//         yield put(userCheckActions.disableLoader());

//         // no need to call navigate as this is handled by redux store with SwitchNavigator
//         yield call(navigationActions.navigateToHome);
//     } else {
//         // console.log(response.data);
//         yield put(userCheckActions.userCheckFailed());
//         yield put(userCheckActions.disableLoader());
//         setTimeout(() => {
//             Alert.alert('BoilerPlate', response.message);
//         }, 200);
//     }
// }
