/**
 *  Redux saga class init
 */
import { takeEvery, all } from 'redux-saga/effects';
import * as LoadingTypes from '@constants/login.constant';
import * as UserCheckTypes from '@constants/user-check.constant';
import loginSaga from '@sagas/login.saga';
// import userCheckSaga from '@sagas/user-check.saga';
import { requestLogin } from '@reducers/auth.reducer';

export default function* watch() {
    yield all([
        takeEvery(requestLogin().type, loginSaga),
        // takeEvery(UserCheckTypes.USER_CHECK_REQUEST, userCheckSaga),
    ]);
}
