/* Redux saga class
 * logins the user into the app
 * requires email and password.
 * un - email
 * pwd - password
 */
import { put, call, select } from 'redux-saga/effects';
// import { delay } from 'redux-saga';

import { Alert } from 'react-native';

import { LoginApi } from '@services/login.service';
import * as AppStackScreen from '@actions/navigation.actions';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { onLoginResponse, disableLoader, loginFailed, enableLoader } from '@reducers/auth.reducer';


export const getEmail = (state: any) => state.loginReducer.email;
export const getPassword = (state: any) => state.loginReducer.password;

// Our worker Saga that logins the user
export default function* loginAsync() {
    yield put(enableLoader());
    const email: string = yield select(getEmail);
    const password: string = yield select(getPassword);

    // How to call api
    const params = {
        email: email,
        password: password,
        role: 'Company',
    };

    const response = yield call(LoginApi.post, params);
    // Mock response
    // const response = { success: true, data: { id: 1 }, message: 'Success' };
    // console.log(response);

    if (response.isSuccess) {
        yield put(onLoginResponse(response));
        yield put(disableLoader());
        AsyncStorage.setItem('access_token', JSON.stringify(response.data.access_token));

        // no need to call navigate as this is handled by redux store with SwitchNavigator
        yield call(AppStackScreen.bottomTabs);
    } else {
        // console.log(response.data);
        yield put(loginFailed());
        yield put(disableLoader());
        setTimeout(() => {
            Alert.alert('BoilerPlate', response.message);
        }, 200);
    }
}
