/* Login Reducer
 * handles login states in the app
 */
import createReducer from '@utils/createReducer';
import * as UserCheckTypes from '@constants/user-check.constant';
import { IUserCheckRequestState, IUserCheckResponseState, IUserCheckState } from '@models/user-check.model';


const initialState: IUserCheckState = {
    data: {},
};

export const userCheckReducer = createReducer(initialState, {
    [UserCheckTypes.USER_CHECK_REQUEST](state: IUserCheckState, action: IUserCheckRequestState) {
        return {
            ...state,
        };
    },
    [UserCheckTypes.USER_CHECK_LOADING_ENDED](state: IUserCheckState) {
        return { ...state };
    },
    [UserCheckTypes.USER_CHECK_RESPONSE](state: IUserCheckState, action: IUserCheckResponseState) {
        return {
            ...state,
            data: action.response.data.data,
            isSuccess: true,
        };
    },
    [UserCheckTypes.USER_CHECK_FAIL](state: IUserCheckState) {
        return {
            ...state,
            isSuccess: false,
        };
    },
});
