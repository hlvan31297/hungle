/**
 * Loading reducer made separate for easy blacklisting
 * Avoid data persist
 */
import createReducer from '@utils/createReducer';
import * as LoadingTypes from '@constants/loading.constant';
import { ILoading } from '@models/loading.model';

const initialState: ILoading = {
    isLoginLoading: false,
};

export const loadingReducer = createReducer(initialState, {
    [LoadingTypes.LOGIN_ENABLE_LOADER](state: ILoading) {
        return { ...state, isLoginLoading: true };
    },
    [LoadingTypes.LOGIN_DISABLE_LOADER](state: ILoading) {
        return { ...state, isLoginLoading: false };
    },
});
