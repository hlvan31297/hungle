import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
    ILoginState, ILoginRequestState,
    ILoginResponseState,
} from '@models/login.model';

const initialState: ILoginState = {
    isLoggedIn: false,
    email: '',
    password: '',
    access_token: '',
    email_verified: false,
    token_cognito: {
        AccessToken: '',
        RefreshToken: '',
        IdToken: '',
    },
};

const loginSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        requestLogin(state, action: PayloadAction<ILoginRequestState>) {
            return {
                ...state,
                email: action.payload.email,
                password: action.payload.password,
            };
        },
        enableLoader(state: ILoginState) {
            return { ...state };
        },
        disableLoader(state: ILoginState) {
            return { ...state };
        },
        onLoginResponse(state: ILoginState, action: PayloadAction<ILoginResponseState>) {
            return {
                ...state,
                access_token: action.payload.data.access_token,
                email_verified: action.payload.data.email_verified,
                token_cognito: action.payload.data.token_cognito,
                isLoggedIn: true,
            };
        },
        loginFailed(state: ILoginState) {
            return {
                ...state,
                isLoggedIn: false,
            };
        },
        logOut(state: ILoginState) {
            return {
                ...state,
                isLoggedIn: false,
            };
        },
    },
});

export default loginSlice.reducer;
export const { requestLogin, enableLoader, disableLoader, onLoginResponse, loginFailed, logOut } = loginSlice.actions;

export const loginSelector = (state: { loginStore: ILoginState }) =>
  state.loginStore;
