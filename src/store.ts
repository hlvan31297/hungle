import { createStore, compose, applyMiddleware } from 'redux';
import { persistStore, persistCombineReducers } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { combineReducers } from '@reduxjs/toolkit';


import rootReducers from '@reducers/index'; // where reducers is a object of reducers
import sagas from '@sagas/index';


declare global {
    interface Window {
      __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}

const config = {
    key: 'root',
    storage: AsyncStorage,
    blacklist: ['loadingReducer'],
    debug: true, // To get useful logging
};

const middleware = [];
const sagaMiddleware = createSagaMiddleware();

middleware.push(sagaMiddleware);
let composeEnhancers = compose;

if (__DEV__) {
    middleware.push(createLogger());
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ as typeof compose || compose;
}

const reducers = persistCombineReducers(config, rootReducers);
const enhancers = [applyMiddleware(...middleware)];
const initialState = undefined;
const persistConfig: any = { enhancers };
const store = createStore(reducers, initialState, composeEnhancers(...enhancers));
const persistor = persistStore(store, persistConfig, () => {
    // console.log('Test', store.getState());
});
const configureStore = () => {
    return { persistor, store };
};

sagaMiddleware.run(sagas);

export default configureStore;
export type RootState = ReturnType<typeof reducers>;
