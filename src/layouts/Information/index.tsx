import React, { useRef, useState } from 'react';
import {
  Text,
  Dimensions,
  Image,
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  Platform
} from 'react-native';
import styles from './style'
import { Layout, Tab, TabView, } from '@ui-kitten/components';
import Registered from './Registered';
import Post from './Post'
export default ({ navigation }): React.ReactElement => {
    const [currentSlide, setCurrentSlide] = useState(0);
    const [selectedIndex, setSelectedIndex] = React.useState(0);
    return(
        <View style={styles.container}>
            <View style={{marginTop:120,}}>
             <TabView
                selectedIndex={selectedIndex}
                onSelect={index => setSelectedIndex(index)}
                
                >
                <Tab title='登録学生'>
                    <Layout style={styles.scrollview}>
                        <Registered />
                    </Layout>
                </Tab>
                <Tab title='投稿'>
                    <Layout style={styles.scrollview}>
                        <Post/>
                    </Layout>
                </Tab>
            </TabView>
            </View>
        </View>
    )
};
