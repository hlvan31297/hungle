import {IMAGE} from '../../components/image';
import { ImageSourcePropType } from 'react-native';
export  const data: image[] = [
    {
      image: IMAGE.INTRO_8,
      id: 1,

    },
    {
      image: IMAGE.INTRO_8,
      id: 2,

    },
    {
      image: IMAGE.INTRO_8,
      id: 3,

    },
    {
      image: IMAGE.INTRO_8,
      id: 4,

    },
    {
        image: IMAGE.INTRO_8,
        id: 5,
  
    },
    {
        image: IMAGE.INTRO_8,
        id: 6,
  
    },
    {
        image: IMAGE.INTRO_8,
        id: 7,
  
    },
    {
        image: IMAGE.INTRO_8,
        id: 8,
  
    },
    {
        image: IMAGE.INTRO_8,
        id: 9,
  
    },
    {
        image: IMAGE.INTRO_8,
        id: 10,
  
    },
    {
        image: IMAGE.INTRO_8,
        id: 11,
  
    },
    {
        image: IMAGE.INTRO_8,
        id: 12,
  
    },
    {
        image: IMAGE.INTRO_8,
        id: 13,
  
    },
];
export const dataHome : Home []= [
  {
    image: IMAGE.INTRO_8,
    id: 1,
    name: `Lorem Ipsum株式会社`,
    title: `慶應義塾大学経済学部 / 2年生/ ボート部`,
  },
  {
    image: IMAGE.INTRO_8,
    id: 2,
    name: `Lorem Ipsum株式会社`,
    title: `慶應義塾大学経済学部 / 2年生/ ボート部`,
  },
  {
    image: IMAGE.INTRO_8,
    id: 3,
    name: `Lorem Ipsum株式会社`,
    title: `慶應義塾大学経済学部 / 2年生/ ボート部`,
  },
  {
    image: IMAGE.INTRO_8,
    id: 4,
    name: `Lorem Ipsum株式会社`,
    title: `慶應義塾大学経済学部 / 2年生/ ボート部`,
  },
  {
    image: IMAGE.INTRO_8,
    id: 5,
    name: `Lorem Ipsum株式会社`,
    title: `慶應義塾大学経済学部 / 2年生/ ボート部`,
  },
  {
    image: IMAGE.INTRO_8,
    id: 6,
    name: `Lorem Ipsum株式会社`,
    title: `慶應義塾大学経済学部 / 2年生/ ボート部`,
  },
  {
    image: IMAGE.INTRO_8,
    id: 7,
    name: `Lorem Ipsum株式会社`,
    title: `慶應義塾大学経済学部 / 2年生/ ボート部`,
  },
  {
    image: IMAGE.INTRO_8,
    id: 8,
    name: `Lorem Ipsum株式会社`,
    title: `慶應義塾大学経済学部 / 2年生/ ボート部`,
  },
  {
    image: IMAGE.INTRO_8,
    id: 9,
    name: `Lorem Ipsum株式会社`,
    title: `慶應義塾大学経済学部 / 2年生/ ボート部`,
  },
  {
    image: IMAGE.INTRO_8,
    id: 10,
    name: `Lorem Ipsum株式会社`,
    title: `慶應義塾大学経済学部 / 2年生/ ボート部`,
  },
  {
    image: IMAGE.INTRO_8,
    id: 11,
    name: `Lorem Ipsum株式会社`,
    title: `慶應義塾大学経済学部 / 2年生/ ボート部`,
  },
];



export interface image {
    image : ImageSourcePropType;
    id : number ;
}
export interface Home {
  image : ImageSourcePropType;
  id: number ;
  name: string;
  title: string;
}