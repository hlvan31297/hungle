import React, { useRef, useState } from 'react';
import {
    Text,
    Dimensions,
    Image,
    StyleSheet,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
// import styles from './style';
import { data } from './data';
const Post = () => {
    return (
            <ScrollView>
                <View style={styles.scrollview}>
                        {data.map((item, index) =>
                            <Image
                                source={item.image}
                                style={styles.image}
                            />
                        )}
                </View>
            </ScrollView>
    );
};
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ffff',
        flexDirection:'row',
    },
    scrollview: {
        flexDirection:'row',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        flexWrap: 'wrap',
    },
    image: {
        height: Dimensions.get('window').width/3,
        width: Dimensions.get('window').width/3,
        fontWeight: 'bold',
        
    },
})
export default Post;