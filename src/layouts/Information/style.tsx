import { StyleSheet, Dimensions } from 'react-native';
const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F4F6F8',
        borderRadius: 40,
        // borderWidth:2,
        borderColor: '#ae22e0'
    },
    scrollview: {
        // marginTop: 20,
        backgroundColor: '#ffff',
    },
    viewbody: {
        padding:30,
        flexDirection: 'row',
    },
    name: {
        alignSelf: 'center',
        marginLeft: 10,
        fontWeight: 'bold',
    },
    image: {
        height: 60,
        width: 60,
        borderRadius: 40,
        // borderWidth: 3,
        borderColor: '#ae22e0',
    },
});
export default style ;