import React, { useRef, useState } from 'react';
import {
    Text,
    Dimensions,
    Image,
    StyleSheet,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import styles from './style';
import { dataHome } from './data';
const Registered = () => {
    return (
        <View>
            <ScrollView>
                { dataHome.map((item, index) => 
                <View style={styles.scrollview}>
                    <View style={styles.viewbody}>
                        <Image
                            source={item.image}
                            style={styles.image}
                        />
                        <View style={{marginLeft:20}}>
                           <View style={{marginLeft:-110}}><Text style={styles.name}>{item.name}</Text></View>
                        <View style={{marginLeft:5,marginTop:7,}}><Text>{item.title}</Text></View>
                        </View>
                    </View>
                    </View>
                )} 
            </ScrollView>
        </View>
    );
};
export default Registered ;