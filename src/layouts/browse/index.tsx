import React, { useRef, useState } from 'react';
import {
    Text,
    Dimensions,
    Image,
    StyleSheet,
    View,
    ScrollView,
    TouchableOpacity,
    TextInput,
    FlatList,
    Animated,
    StatusBar,
} from 'react-native';
// import styles from './extra/style';
import { dataHome } from './extra/data';
import { Layout, Tab, TabView, } from '@ui-kitten/components';
import { Screen } from '../../navigation/index';
export default ({ navigation }): React.ReactElement => {

    const [email, onChangeEmail] = React.useState();
    const [selectedIndex, setSelectedIndex] = React.useState(0);
    return (
        <View style={styles.container}>
            <View style={styles.viewInput}>
                <TextInput
                    style={styles.inputStyle}
                    onChangeText={() => onChangeEmail}
                    value={email}
                    placeholder='Search Keywoard'
                />
            </View>
            <View>
                    <ScrollView>
                        {dataHome.map((item, index) =>
                            <View style={styles.scrollview}>
                                <View style={styles.viewbody}>
                                    <Image
                                        source={item.image}
                                        style={styles.image}
                                    />
                                    <Text style={styles.name}>{item.name}</Text>
                                </View>
                                <View>
                                    <Image source={require('./assets/image-background-post-2.jpg')}
                                        style={styles.imagebody} />
                                </View>
                            </View>
                        )}
                    </ScrollView>
                </View>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFF',
        borderRadius: 40,
        borderWidth:2,
        borderColor: '#ae22e0',
    },
    viewInput: {
        marginTop: 50,
        borderWidth:2,
        borderColor: '#EEEEEE',
        marginLeft:16,
        marginRight: 50,
        borderRadius: 20,
        height:'7%',
        width:'90%',
        justifyContent: 'center',

    },
    inputStyle: {
        // width: '100%',
        marginHorizontal: 30,
        fontSize: 15,
        color: '#263238',
        // textAlign: 'center'
    },
    styleimage:{
        padding:10,
        borderWidth: 1,
        borderColor: '#EEEEEE',
        marginTop: 23,
        backgroundColor: '#ffff',
        marginLeft: 16,
        marginRight: 18,
    },
    scrollview: {
        // marginTop: 20,
        backgroundColor: '#ffff',
    },
    viewbody: {
        padding:10,
        flexDirection: 'row',
    },
    image: {
        height: 40,
        width: 40,
        borderRadius: 40,
        // borderWidth: 3,
        borderColor: '#ae22e0',
    },
    name: {
        alignSelf: 'center',
        marginLeft: 10,
        fontWeight: 'bold',
    },
    imagebody: {
        height: Dimensions.get('window').width/2,
        width: Dimensions.get('window').width,
    },
    texttitle: {
        fontSize: 13,
    },
    tabContainer: {
        height: 64,
        alignItems: 'center',
        justifyContent: 'center',
      },
});

