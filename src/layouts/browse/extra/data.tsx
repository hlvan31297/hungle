import { IMAGE } from '../../../components/image';
import { ImageSourcePropType } from 'react-native';
export const dataHome: Home[] = [
    {
        image: IMAGE.INTRO_8,
        id: 1,
        name: `Lorem Ipsum株式会社`,
        Time: `50分前`,
        like: `100件のいいね`,
        comment: `10件のコメント`,
        title_comment: `投稿の内容が入ります投稿の内容が入ります投稿の内容が入ります投稿の内容が入ります投稿の内容が入ります投稿…more`,
    },
    {
        image: IMAGE.INTRO_8,
        id: 2,
        name: `Lorem Ipsum株式会社`,
        Time: `20分前`,
        like: `100件のいいね`,
        comment: `10件のコメント`,
        title_comment: `投稿の内容が入ります投稿の内容が入ります投稿の内容が入ります投稿の内容が入ります投稿の内容が入ります投稿…more`,
    },
    {
        image: IMAGE.INTRO_8,
        id: 3,
        name: `Lorem Ipsum株式会社`,
        Time: `20分前`,
        like: `100件のいいね`,
        comment: `10件のコメント`,
        title_comment: `投稿の内容が入ります投稿の内容が入ります投稿の内容が入ります投稿の内容が入ります投稿の内容が入ります投稿…more`,
    },
    {
        image: IMAGE.INTRO_8,
        id: 4,
        name: `Lorem Ipsum株式会社`,
        Time: `20分前`,
        like: `100件のいいね`,
        comment: `10件のコメント`,
        title_comment: `投稿の内容が入ります投稿の内容が入ります投稿の内容が入ります投稿の内容が入ります投稿の内容が入ります投稿…more`,
    },
    {
        image: IMAGE.INTRO_8,
        id: 5,
        name: `Lorem Ipsum株式会社`,
        Time: `20分前`,
        like: `100件のいいね`,
        comment: `10件のコメント`,
        title_comment: `投稿の内容が入ります投稿の内容が入ります投稿の内容が入ります投稿の内容が入ります投稿の内容が入ります投稿…more`,
    },
];
export interface Home {
    image: ImageSourcePropType;
    id: number;
    name: string;
    Time: string;
    like: string;
    comment: string;
    title_comment: string;
};
export interface name {
    title: string;
}