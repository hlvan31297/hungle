import {IMAGE} from '../../../components/image';
import { ImageSourcePropType } from 'react-native';
export  const data: image[] = [
    {
      image: IMAGE.INTRO_1,
      id: 1,
      name: `hihi`,
    },
    {
      image: IMAGE.INTRO_2,
      id: 2,
      name: `hihi`,
    },
    {
      image: IMAGE.INTRO_3,
      id: 3,
      name: `hihi`,
    },
    {
      image: IMAGE.INTRO_8,
      id: 4,
      name: `hihi`,
    },
];
export const dataTag : Tag []= [
  {
    id:1,
    name: `Tag1`
  },
  {
    id:2,
    name: `Tag2`
  },
  {
    id:3,
    name: `Tag3`
  },
  {
    id:4,
    name: `Tag4`
  },
  {
    id:5,
    name: `Tag5`
  },
]
export const dataHome : Home []= [
  {
    image: IMAGE.INTRO_8,
    id: 1,
    name: `Lorem Ipsum株式会社`,
    Time: `50分前`,
    like: `Contents name`,
    comment: `5.5`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_8,
    id: 2,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `2.3`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_8,
    id: 3,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `4.1`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_8,
    id: 3,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `3.5`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_8,
    id: 3,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `3.5`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_8,
    id: 3,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `3.5`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_3,
    id: 3,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `3.5`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_3,
    id: 3,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `3.5`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_3,
    id: 3,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `3.5`,
    title_comment: `extra information`,
  },
]

export const dataHome1 : Home []= [
  {
    image: IMAGE.INTRO_3,
    id: 1,
    name: `Lorem Ipsum株式会社`,
    Time: `50分前`,
    like: `Contents name`,
    comment: `5.5`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_3,
    id: 2,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `2.3`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_3,
    id: 3,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `4.1`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_3,
    id: 3,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `3.5`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_3,
    id: 3,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `3.5`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_3,
    id: 3,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `3.5`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_3,
    id: 3,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `3.5`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_3,
    id: 3,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `3.5`,
    title_comment: `extra information`,
  },
  {
    image: IMAGE.INTRO_3,
    id: 3,
    name: `Lorem Ipsum株式会社`,
    Time: `20分前`,
    like: `Contents name`,
    comment: `3.5`,
    title_comment: `extra information`,
  },
]


export interface image {
    image : ImageSourcePropType;
    id : number ;
    name: string;
}
export interface Home {
  image : ImageSourcePropType;
  id: number ;
  name: string;
  Time: string;
  like: string;
  comment: string;
  title_comment: string;
}
export interface Tag {
  id: number;
  name: string;
}