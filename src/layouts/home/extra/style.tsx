import { StyleSheet, Dimensions } from 'react-native';
const { height } = Dimensions.get('window');
    var box_count = 2;
    var box_height = height / box_count;
const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F4F6F8',
        borderRadius: 40,
        // borderWidth:2,
        borderColor: '#ae22e0',
        flexDirection: 'column'
    },
    styleImage:{
        padding:10,
        borderRadius: 40,
        borderWidth: 3,
        borderColor: '#ae22e0',
        marginTop:40,
        backgroundColor: '#ffff',
     },
    image: {
        height: 40,
        width: 40,
        borderRadius: 40,
        // borderWidth: 3,
        borderColor: '#ae22e0',
    },
    viewBody: {
        padding:10,
        flexDirection: 'row',
    },
    name: {
        alignSelf: 'center',
        marginLeft: 10,
        fontWeight: 'bold',
    },
    time: {
        alignSelf: 'center',
        marginLeft: 50,
    },
    imageBody: {
        height: Dimensions.get('window').width/2,
        width: Dimensions.get('window').width,
    },
    dotActive: {
        margin: 2,
        color: 'blue',
        fontSize: 7,
    },
    dot: {
        margin: 2,
        color: '#888',
        fontSize: 7,
    },
    wrapDot: {
        bottom: -16,
        flexDirection: 'row',
        alignSelf: 'center',
    },

    viewLike: {
        marginLeft: 16,
        flexDirection: 'row',
        marginTop: 15,
    },
    scrollView: {
        marginTop: 10,
        backgroundColor: '#ffff',
    },
    iconButton: {
        paddingHorizontal: 0,
    },
    ViewTag: {
        // marginLeft: 15,
        // display: 'flex',
    },
    Tag:{
        width: '80%',
        height: '70%',
        backgroundColor: '#7583CA',
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: '#FF4000',
        fontWeight: 'bold',
        // marginTop:15,
        // marginLeft: 3,
        display: 'flex',
    },
    textTag: {
        fontWeight: 'bold',
        // marginLeft: 10,
        textAlign: 'center'
    },
    styleInput: {
        marginTop: 50,
        width: '80%',
        height: '40%',
        marginLeft: 100,
        // flexDirection: 'row',
        borderColor:'#8B0016',
        flexDirection: 'row',
    },
    buttonBottom: {
        width: '30%',
        height: '100%',
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: '#A1A4B2',
        fontWeight: 'bold',
    },
    textIdRequest: {
        fontWeight: 'bold',
        marginLeft: 10,
        color: '#000000',
    },
    btn: {
        // marginLeft: 100,
        width: '32%',
        height: '100%',
        // backgroundColor: '#000000',
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: '#A1A4B2',
        fontWeight: 'bold',
    },
    box: {
        flex: 1
    },
      box1: {
        flex: 1.5,
        backgroundColor: '#F4F6F8'
    },
    //content
    box2: {
        flex: 10,
        backgroundColor: '#F4F6F8'
    },
    //footer
});
export default style ;