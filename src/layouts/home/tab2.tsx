import React, { useRef, useState } from 'react';
import {
    Text,
    Dimensions,
    Image,
    StyleSheet,
    View,
    ScrollView,
    FlatList,
    TouchableOpacity,
} from 'react-native';
import { data } from './extra/data';
import styles from './extra/style';
import { dataHome1 } from './extra/data';
import { dataTag } from './extra/data'
const tab2 = () => {
    const [currentSlide, setCurrentSlide] = useState(0);
    const onchange = (nativeEvent) => {
        if (nativeEvent) {
            const slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width);
            if (slide != currentSlide) {
                setCurrentSlide(slide);
            }
        }
    };
    return (
        <View style={{ backgroundColor: '#F4F6F8', }}>
            <View>
                <Text style={{textAlign: 'center',color: '#000000'}}>Search Keywoard</Text>
            </View>
            <View style={{ marginTop: 26 }}>
                <ScrollView
                    horizontal={true}
                    // nestedScrollEnabled={true}
                    // showsHorizontalScrollIndicator={false}
                    // pagingEnabled
                    // scrollEventThrottle={16}
                    style={{ height: '5%', marginLeft: 28 }}
                >

                    {
                        dataTag.map((item, index) =>
                            <View style={styles.ViewTag}>
                                <TouchableOpacity
                                    style={styles.Tag}>
                                    <Text style={styles.textTag}>
                                        {item.name}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        )
                    }

                </ScrollView>
            </View>
            <ScrollView>
                {
                    dataHome1.map((item, index) =>
                        <View style={styles.scrollView}>
                            <View style={styles.viewBody}>
                            </View>
                            <View>
                                <Image source={item.image}
                                    style={styles.imageBody} />

                            </View>
                            <View style={styles.viewLike}>
                                <Text style={{ color: '#000000' }}>{item.like}</Text>
                                <Text style={{ marginLeft: 250, color: '#000000' }}>{item.comment}</Text>
                            </View>
                            <View style={{ marginLeft: 16, marginTop: 5, }}><Text style={{ color: '#6D6D6D' }}>{item.title_comment}</Text></View>
                        </View>
                    )
                }
            </ScrollView>
        </View>

    );
};
export default tab2;