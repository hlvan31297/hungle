import React, { useRef, useState } from 'react';
import {
    Text,
    Dimensions,
    Image,
    StyleSheet,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import { data } from './extra/data';
import styles from './extra/style';
import tab1 from './tab1'
import tab2 from './tab2'
import { createStackNavigator } from '@react-navigation/stack';
import { useTranslation } from 'react-i18next';
import { Screen } from '@navigation/index';

export default ({ navigation }): React.ReactElement => {
    const { t, i18n } = useTranslation('trans');
    const Stack = createStackNavigator();
    const [componentRender, setComponentRender] = useState(0);
    const [selectedIndex, setSelectedIndex] = React.useState(0);
    const [active, setActive] = useState(true);
    const [active1, setActive1] = useState(false);
    const onTab1 = () => {
        setComponentRender(0)
    }
    
    const onTab2 = () => {
        setComponentRender(1)
    };

    return (
        <View style={styles.container}>
            <View style={[styles.box, styles.box1]}>
                <View style={styles.styleInput} >
                    <TouchableOpacity
                        onPress={onTab1}
                        style={{ 
                        width: '30%',
                        height: '100%',
                        borderRadius: 30,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 1,
                        borderColor: '#A1A4B2',
                        backgroundColor: componentRender === 0 ? '#000000' : '#ffffff' }}
                    >
                        <Text style={{
                             fontWeight: 'bold',
                             marginLeft: 10,
                             color: componentRender === 0 ? '#ffffff' : '#000000',
                        }}>
                            {t("Tab1").toString()}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={onTab2}
                        style={{width: '32%',
                        height: '100%',
                        backgroundColor: componentRender === 1 ? '#000000' : '#ffffff',
                        borderRadius: 30,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 2,
                        borderColor: '#A1A4B2',}}
                    >
                        <Text style={{
                             fontWeight: 'bold',
                             marginLeft: 10,
                             color: componentRender === 1 ? '#ffffff' : '#000000',
                        }}>
                            {t("Tab2").toString()}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
            {
                componentRender === 0 ?
                    <View style={[styles.box, styles.box2]}>
                        <Stack.Navigator>
                            <Stack.Screen name='h123'
                                component={tab1}
                                options={{
                                    headerShown: false,
                                }} />

                        </Stack.Navigator>
                    </View>
                    :
                    <View style={[styles.box, styles.box2]}>
                        <Stack.Navigator>
                            <Stack.Screen name='h234'
                                component={tab2}
                                options={{
                                    headerShown: false,
                                }} />
                        </Stack.Navigator>
                    </View>
            }
        </View>
    )
};
