import { ImageSourcePropType } from 'react-native';
export interface slides {
    title: string;
    image : ImageSourcePropType;
    button : string;
    id : number ;
  }