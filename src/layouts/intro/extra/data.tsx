import {IMAGE} from '../../../components/image';
import {slides } from './type';
   const lineBreak = '\n';
 export  const data: slides[] = [
    {
      title: `stairは、高学歴かつアスリート${lineBreak}学生に限定した新しい${lineBreak}就活プラットフォームです。`,
      image: IMAGE.INTRO_1,
      button: '次へ',
      id: 1,
    },
    {
      title: `SNSを通じて、1年生から${lineBreak}企業にアピールすることが${lineBreak}でき、企業からもオファーが${lineBreak}届きます。`,
      image: IMAGE.INTRO_2,
      button: '次へ',
      id: 2,
    },
    {
      title: `学生同士も友達として${lineBreak}つながることができます。${lineBreak}それが、未来の人脈形成に${lineBreak}大きく役立つかもしれません。`,
      image: IMAGE.INTRO_3,
      button: '次へ',
      id: 3,
    },
    {
      title: `熱狂する力を社会へ！${lineBreak}高学歴アスリート学生を求める${lineBreak}企業とエンゲージメントを深め${lineBreak}あなたにとって最適な${lineBreak}就職先を見つけましょう！`,
      image: IMAGE.INTRO_4,
      button: 'さあ、はじめましょう',
      id: 4,
    },
    ];