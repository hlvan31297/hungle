import React, { useRef, useState } from 'react';
import {
  Text,
  Dimensions,
  Image,
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  Platform
} from 'react-native';
import { data } from './extra/data';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Screen } from '../../navigation/index';
const { width: screenWidth, height: screenHeight } = Dimensions.get('window');
export default ({ navigation }): React.ReactElement => {
  const Drawer = createDrawerNavigator();
  const scrollRef = useRef<ScrollView>(null);
  const [currentSlide, setCurrentSlide] = useState(0);
  const onchange = (nativeEvent) => {
    if (nativeEvent) {
      const slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width);
      if (slide != currentSlide) {
        setCurrentSlide(slide);
      }
    }
  };
  const handleScrollEnd = (e: any) => {
    if (!e) {
      return
    }
    const { nativeEvent } = e
    if (nativeEvent && nativeEvent.contentOffset) {
      let currentSlide = 1
      if (nativeEvent.contentOffset.x === 0) {
        setCurrentSlide(currentSlide)
      } else {
        const approxCurrentSlide = nativeEvent.contentOffset.x / screenWidth
        currentSlide = Math.ceil(parseInt(approxCurrentSlide.toFixed(2)) + 1)
        setCurrentSlide(currentSlide)
      }
    }
  }
  const onNextButtonPress = (idButton) => {
    if (idButton === 4) {
      console.log(Screen)
      navigation.navigate(Screen.survey)
    } else {
      const scrollPoint = (currentSlide + 1) * screenWidth
      scrollRef?.current.scrollTo({
        x: scrollPoint,
        y: 0,
        animated: true
      })
      if (Platform.OS === 'android') {
        handleScrollEnd({ nativeEvent: { contentOffset: { y: 0, x: scrollPoint } } })
      }
    }
  }
  return (
    <View style={styles.container}>
      <ScrollView
        ref={scrollRef}
        onScroll={({ nativeEvent }) => onchange(nativeEvent)}
        horizontal
        nestedScrollEnabled={true}
        decelerationRate={0.1}
        showsHorizontalScrollIndicator={false}
        pagingEnabled
        scrollEventThrottle={16}
      >
        {
          data.map((item, _) =>
            <View key={item.id}>
              <Image
                source={item.image}
                style={styles.wSap}
              />
              <View style={styles.child}>
                <Text style={styles.textTitleStyle}>{item.title}</Text>
              </View>

              <View style={styles.viewBottom}>

                <TouchableOpacity
                  style={styles.signInButton}
                  onPress={() => onNextButtonPress(item.id)}
                >
                  <Text style={styles.textButtonStyle}>
                    {item.button}
                  </Text>
                </TouchableOpacity>

              </View>
            </View>
          )
        }
      </ScrollView>
      <View style={styles.wrapDot}>
        {
          data.map((item, index) =>
            <Text
              key={item.id}
              style={currentSlide == index ? styles.dotActive : styles.dot}
            >
              ●
            </Text>
          )
        }
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "black",
  },


  image: {
    ...StyleSheet.absoluteFillObject,
    width: undefined,
    height: undefined,
  },
  wSap: {
    width: screenWidth,
    height: screenHeight,
  },
  wrapDot: {
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    alignSelf: 'center',
  },
  dotActive: {
    margin: 3,
    color: '#fff'
  },
  dot: {
    margin: 3,
    color: '#888'
  },
  textTitleStyle: {
    fontSize: screenWidth < 370 ? 22 : 24,
    color: '#fff',
    textAlign: 'center',
    fontWeight: '400',
    position: 'relative',
  },

  child: {
    position: 'absolute',
    marginTop: 100,
    flex: 1,
    alignItems: 'center',
    marginHorizontal: 5,
    width: '100%',
    display: 'flex',
    justifyContent: 'center'
  },
  textButtonStyle: {
    fontSize: screenWidth < 370 ? 15 : 17,
    color: '#fff',
    textAlign: 'center',
    paddingVertical: 15,
    paddingHorizontal: 60,
    fontWeight: '400',
    opacity: 1,

  },
  signInButton: {
    backgroundColor: '#2680EB',
    alignSelf: 'center',
    borderRadius: 20,
    alignItems: 'center',
  },
  viewBottom: {
    alignItems: 'center',
    position: 'absolute',
    display: 'flex', width: '100%',
    justifyContent: 'center',
    left: 0,
    bottom: 100
  }
});
