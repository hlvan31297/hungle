import React from 'react';
import { StyleSheet, View, Text, ScrollView, TextInput, Image, TouchableOpacity } from 'react-native';
import { IndexPath, Layout, Select, SelectGroup, SelectItem } from '@ui-kitten/components';
import { Screen } from '../../navigation/index';
import {
    BottomTabNavigationOptions,
    createBottomTabNavigator,
  } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Foundation';
import { home } from '@scenes/home/home.components';
import { browse } from '@src/scenes/browse/browse.components';
import { InformationScreen } from '@scenes/Information/Information.components';
import { messageScreen } from '@scenes/messaging/message.component';
import { myPage } from '@scenes/myPage/myPage.components';
import { useTranslation } from 'react-i18next';
import { ColorPaletteIcon, LayoutIcon, StarOutlineIcon } from '@components/icons';

export default ({ navigation }): React.ReactElement => {
  const { t, i18n } = useTranslation('trans');
  const Tab = createBottomTabNavigator();
        return (
          <Tab.Navigator
          tabBarOptions={{
            showLabel: false,
            style: {
              ...styles.shadow,
            },
          }}
          >
            <Tab.Screen name='home' component={home} options={{
              tabBarIcon: ({focused}) => (
                <View>
                  <Icon name = 'home' size={20}/>
                  <Text
                  style={{color: focused ? '#e32f45' : '#748c84' }}>{t("Home").toString()}</Text>
                </View>
              ),
            }} />
            <Tab.Screen name='Browse' component={browse} options={{
              tabBarIcon: ({focused}) => (
                <View>
                  <Icon name = 'home' size={20}/>
                  <Text
                  style={{color: focused ? '#e32f45' : '#748c84' }}>{t("browse").toString()}</Text>
                </View>
              ),
            }}/>
            <Tab.Screen name='学生情報' component={InformationScreen} options={{
              tabBarIcon: ({focused}) => (
                <View style={{alignItems: 'center', justifyContent: 'center', top: 5}}>
                  <Icon name = 'home' size={20}/>
                  <Text
                  style={{color: focused ? '#e32f45' : '#748c84' }}>{t("chat").toString()}</Text>
                </View>
              ),
            }}/>
            <Tab.Screen name='メッセージ' component={messageScreen} options={{
              tabBarIcon: ({focused}) => (
                <View>
                  <Icon name = 'home'
                  size= {20}
                  style={{
                    width: 25,
                    height: 25,
                  }}/>
                  <Text
                  style={{color: focused ? '#e32f45' : '#748c84', fontSize: 12}}>{t("Sign up").toString()}</Text>
                </View>
              ),
            }}/>
            <Tab.Screen name='マイページ' component={myPage} options={{
              tabBarIcon: ({focused}) => (
                <View>
                  <Icon name = 'home' size={20}/>
                  <Text
                  style={{color: focused ? '#e32f45' : '#748c84' }}>{t("Account").toString()}</Text>
                </View>
              ),
            }}/>
          </Tab.Navigator>
        );
}

const styles = StyleSheet.create({
    shadow: {
      shadowColor: '#7F5DF0',
      shadowOffset: {
        width: 0,
        height: 10,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.5,
      elevation: 5,
    },
  });
