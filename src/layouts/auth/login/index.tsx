import React, { useRef, useState } from 'react';
import {
    Text,
    StyleSheet,
    View,
    TouchableOpacity,
    TextInput,
    Image
} from 'react-native';
import { requestLogin } from '@reducers/auth.reducer';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { ILoginRequestState, ILoginState } from '@models/login.model';
import { TrophyOutlined } from '@ant-design/icons';
import { KeyboardAvoidingView } from "./extra/3rd-party";
export default ({ navigation }): React.ReactElement => {

    const [email, onChangeEmail] = React.useState("");
    const [password, onChangePassword] = React.useState("");
    const dispatch = useDispatch();
    const { t, i18n } = useTranslation('trans');
    const onMissingIdPress = () => {
        console.log("ID（メールアドレス）を忘れた場合");
        // navigation.navigate(Screen.RequestScreen)
    };
    const onForgotPasswordButtonPress = () => {
        console.log("パスワードを忘れた場合");
        // navigation.navigate(Screen.RequestScreen)
    };
    const onSignInButtonPress = (): void => {
        const payload: ILoginRequestState = {
            type: '',
            email: email,
            password: password,
            role: 'Company',
        };
        dispatch(requestLogin(payload));
    };
    const onSignUpButtonPress = () => {
        console.log("新規登録はこちら");

    }
    return (
        <KeyboardAvoidingView style={styles.container}>
            <Text style={styles.textTile}>{t("Log Into My Account").toString()}</Text>
            <View style={{ marginTop: 40 }}>
                <TouchableOpacity
                    onPress={() => onMissingIdPress()}
                    style={styles.bottomGoogle}
                >
                    <Image
                        style={styles.styleImage}
                        source={require('./assets/google.png')}
                    />
                    <Text style={styles.textIdRequest}>
                        {t("Sign in with Google").toString()}
                  </Text>
                </TouchableOpacity>
            </View>
            <View style={{marginTop: 50}}>
                <Text style={{textAlign: 'center',}}>
                <Text style={{color:'#6D6D6D'}}>───────────</Text>
                <Text style={{fontSize:18}}> {t("or").toString()} </Text>
                <Text style={{color:'#6D6D6D'}}>───────────</Text></Text>
            </View>
            <View style={{ marginTop: 50 }}>
                <View style={styles.formContainer}>
                    <TextInput
                        style={styles.inputStyle}
                        onChangeText={onChangeEmail}
                        value={email}
                        placeholder='Email'
                    />
                </View>
                <View style={styles.formContainer}>
                    <TextInput
                        secureTextEntry={true}
                        style={styles.inputStyle}
                        onChangeText={onChangePassword}
                        value={password}
                        placeholder='Password'
                    />
                </View>
            </View>
            <View style={{marginTop: 10, marginLeft: 280}}>
                    <Text style={styles.textIdRequest}>
                      {t("Forget password").toString()}
                  </Text>
            </View>
            <View style={{ marginTop: 40, marginLeft:24}}>
                <TouchableOpacity
                    onPress={() => onSignInButtonPress()}
                    style={styles.signInButton}
                >
                        <Text style={styles.textButtonSignIn}>
                         {t("Sign in").toString()}
                      </Text>
                </TouchableOpacity>
            </View>
            <View style={{ marginTop:250 }}>
                <TouchableOpacity>
                <Text style={{ textAlign: 'center',}}> {t("Don't have an accounts?").toString()}
                <Text style={{color:'#FC6B68',fontWeight: 'bold'}}>{t("Sign up").toString()}</Text></Text>
                </TouchableOpacity>
            </View>
        </KeyboardAvoidingView>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    textTile: {
        color: '#263238',
        marginTop: 60,
        textAlign: 'center',
        fontSize: 22,
        fontWeight: 'bold'
    },
    formContainer: {
        width: '88%',
        marginTop: 10,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 4,
        borderRadius: 20,
        backgroundColor: '#ffffff',
        // marginHorizontal: 16,
        borderColor: '#585858',
        borderWidth: 1,
        flexDirection: 'row',
        marginLeft: 24
    },
    inputStyle: {
        width: '85%',
        marginHorizontal: 30,
        fontSize: 15,
        color: '#263238',
        // borderRadius: 24,
    },
    buttonBottomBorder: {
        marginBottom: 5,
    },
    bottomGoogle: {
        marginLeft: 30,
        width: '88%',
        backgroundColor: '#ffffff',
        borderRadius: 24,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#A1A4B2',
        height:60
    },
    textIdRequest: {
        fontSize: 14,
    },
    signInButton: {
        width: '94%',
        height: 60,
        // borderColor: '#FC6B68',
        // borderWidth: 1,
        backgroundColor: '#FC6B68',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 24,
    },
    textButtonSignIn: {
        color: '#fff',
        fontSize: 16,
        fontWeight: '700',
        textAlign: 'center',
    },
    styleImage: {
        position: 'absolute',
        left: 30,
    },

});