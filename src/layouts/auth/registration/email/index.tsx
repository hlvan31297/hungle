import React from 'react';
import { StyleSheet, View, Text, ScrollView, TextInput, Image, TouchableOpacity } from 'react-native';
import { IndexPath, Layout, Select, SelectGroup, SelectItem } from '@ui-kitten/components';
import { Screen } from '@navigation/index';
import { useTranslation } from 'react-i18next';
import { KeyboardAvoidingView } from "./extra/3rd-party";
export default ({ navigation }): React.ReactElement => {
    const { t, i18n } = useTranslation('trans');
    const [name, onChangeName] = React.useState("");
    const [selectedIndex, setSelectedIndex] = React.useState(new IndexPath(0));
    const [selectedIndex_two, setSelectedIndex_two] = React.useState(new IndexPath(0));
    const renderOption = (title, key) => (
        <SelectItem key={key} title={title.title} />
    );
    const onGoBack = () => {
        // console.log("backkk");

    }
    const onMissingIdPress = () => {
        navigation.navigate(Screen.password)
    };
    return (
        <KeyboardAvoidingView style={styles.container}>
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={{ flex: 1 }}>
                <Text style={styles.title_1}>
                {t("Find and connect").toString()} {"\n"}{t("neighbors").toString()}
                    </Text>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 80 }}>
                    <Image
                        style={styles.styleImage}
                        source={require('./assets/yogi.png')}
                    />
                </View>
                <View style={{ marginTop: 100, marginLeft: 40, display: 'flex', }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{t("And your email?").toString()}</Text>
                    <View style={styles.formContainer}>
                        <TextInput
                            style={styles.inputStyle}
                            onChangeText={onChangeName}
                            value={name}
                            placeholder='loremipsum@sample.com'
                        />
                    </View>
                </View>
                <View style={styles.styleInput} >
                    <TouchableOpacity
                        onPress={() => onGoBack()}
                        style={styles.buttonBottomBorder}
                    >
                        <Text style={styles.textIdRequest}>
                            {t("Back").toString()}
                         </Text>
                    </TouchableOpacity>
                    <View style={{ display: 'flex' }}>
                        <TouchableOpacity
                            onPress={() => onMissingIdPress()}
                            style={styles.btn}
                        >
                            <Text style={styles.textIdRequest}>
                                {t("Next").toString()}
                           </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </KeyboardAvoidingView>
    );


};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFEFC',
    },
    title_1: {
        textAlign: 'center',
        marginTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 35,
        fontWeight: 'bold'
    },
    styleImage: {
        width: '80%',
        height: '80%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputStyle: {
        borderRadius: 20,
        width: '80%',
        marginHorizontal: 30,
        fontSize: 15,
        // color: '#ffffff',
        flexDirection: 'row',
    },
    formContainer: {
        width: '90%',
        marginTop: 10,
        height: 48,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 4,
        borderRadius: 20,
        backgroundColor: '#ffffff',
        // marginHorizontal: 16,
        borderColor: '#585858',
        borderWidth: 1,
        flexDirection: 'row',
    },
    buttonBottomBorder: {
        width: '40%',
        height: '25%',
        // backgroundColor: '#7583CA',
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: '#A1A4B2',
        position: 'absolute'
    },
    textIdRequest: {
        fontWeight: 'bold',
        marginLeft: 10,
    },
    styleInput: {
        marginTop: 30,
        width: '80%',
        marginLeft: 40,
        // flexDirection: 'row',
    },
    btn: {
        marginLeft: 200,
        width: '40%',
        height: '50%',
        backgroundColor: '#7583CA',
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: '#A1A4B2',
        fontWeight: 'bold',
    },
    title_5: {
        marginTop: 4,
        color: '#263238',
        fontSize: 13,
        marginLeft: 284,
        // textalign: 'right',
    },
});
