import React, { useRef, useState } from 'react';
import {
    Text,
    TouchableOpacity,
    Image,
    StyleSheet,
    View,
    ScrollView,
    SafeAreaView
} from 'react-native';
import {
    Select,
    SelectElement,
    SelectProps,
    SelectItem,
    SelectGroup,
    IndexPath,
    Layout
} from '@ui-kitten/components';
import { Screen } from '@navigation/index';
import SecondaryButton from './extra/SecondaryButton';
import { useTranslation } from 'react-i18next';
import { KeyboardAvoidingView } from "./extra/3rd-party";
export default ({ navigation }): React.ReactElement => {
    const [selectedIndex, setSelectedIndex] = React.useState(new IndexPath(0));
    const [selectedIndex_two, setSelectedIndex_two] = React.useState(new IndexPath(0));
    const { t, i18n } = useTranslation('trans');
    const onMissingIdPress = () => {
        navigation.navigate(Screen.name)
    };
    const onForgotPasswordButtonPress = () => {
        navigation.navigate(Screen.name)
    };
    const B = (props) => <Text style={{fontWeight: 'bold'}}>{props.children}</Text>
    return (
        <KeyboardAvoidingView style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 256 }}>
                    <Image
                        style={styles.styleImage}
                        source={require('./assets/enjoy.png')}
                    />
                </View>
                <View style={{ marginTop: 180, marginLeft: 70 }}>
                    <Image
                        style={styles.imageLogo}
                        source={require('./assets/Logo.jpg')}
                    />
                    <Text style={styles.Template}>{t("Template").toString()}</Text>
                </View>
                <View style={styles.btnWrapper}>
                    <View style={styles.btnItem}>
                        <SecondaryButton
                            label={'CONTINUE WITH FACEBOOK'}
                            background={'#7583CA'}
                            fontColor={'#ffff'}
                            btnType='FACEBOOK'
                        />
                    </View>
                    <View style={styles.btnItem}>
                        <SecondaryButton
                            label={'CONTINUE WITH GOOGLE'}
                            background={'#ffff'}
                            fontColor={'#3F414E'}
                            btnType='GOOGLE'
                        />
                    </View>
                </View>
                <View>
                <TouchableOpacity
                    onPress={() => onMissingIdPress()}
                    style={styles.buttonBottomBorder}
                >
                    <Text style={styles.textIdRequest}>
                       <Text style={styles.textIdRequest}>{t("By sign in up you accept").toString()} <B>{t("Term of service").toString()}</B>{"\n"}
                        {t("and").toString()}<B>{t("privacy policy.").toString()}</B></Text>
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => onForgotPasswordButtonPress()}
                    style={styles.buttonBottomBorder}
                >
                    <Text style={styles.textIdRequest}>
                      {t(" Have an account?").toString()}<Text style={{color:'#FC6B68', fontWeight: 'bold'}}>{t("Sign in").toString()}</Text>
                  </Text>
                </TouchableOpacity>
                </View>
        </KeyboardAvoidingView>
    );


};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    btnWrapper: {
        marginTop: 80,
    },
    btnItem: {
        marginBottom: 20,
    },
    buttonBottomBorder: {
        marginTop:10,
        
    },
    textIdRequest: {
        textAlign: 'center',
    },
    Template: {
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 40,
        marginTop: 20,
        marginLeft: -20
    },
    imageLogo: {
        position: 'absolute',
        width: 60,
        height: 75,
    },
    styleImage: {
        position: 'absolute',
        width: 250,
        height: 250,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title_two: {
        color: '#263238',
        fontSize: 13,
        marginTop: 30,
        marginLeft: 16,
        marginRight: 16,
        // fontFamily: 'Roboto'
    },
    title_one: {
        color: '#263238',
        marginTop: 33,
        marginLeft: 135,
        fontSize: 22,
    },
    title_three: {
        color: '#263238',
        fontSize: 14,
        marginTop: 10,
        marginLeft: 16,
        marginRight: 16,
    },
    title_four: {
        color: '#263238',
        fontSize: 14,
        marginTop: 10,
        marginLeft: 16,
        marginRight: 16,
    },
    title_five: {
        color: '#263238',
        fontSize: 14,
        marginTop: -30,
        marginLeft: 16,
        marginRight: 16,
    },
    selectStyle: {
        marginLeft: 16,
        borderColor: '#D9D9D9',
        borderRadius: 4,
        borderWidth: 1,
        backgroundColor: '#fff',
        maxHeight: 50,
        marginVertical: 8,
        marginRight: 16,
    },
    signInButton: {
        backgroundColor: '#2680EB',
        alignSelf: 'center',
        alignItems: 'center',
    },
})
