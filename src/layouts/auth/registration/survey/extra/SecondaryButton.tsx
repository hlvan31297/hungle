import React from 'react';
import { View, Text, StyleSheet,TouchableOpacity, Image} from "react-native";
 const SecondaryButton = (props) => {
    const {label, background, btnType, fontColor}=props;
    return(
        <View style={styles.container}>
            <TouchableOpacity style={[styles.btn, {backgroundColor:background}]}>
                <Image style={styles.socialIcon}
                 source={btnType==='FACEBOOK'
                 ?require('../assets/facebook.png')
                :require('../assets/google.png')}/>
                <Text style={[styles.label,{color:fontColor}]}>{label}</Text>
            </TouchableOpacity>
            
        </View>
    );
};
const styles = StyleSheet.create({
    container :{
        display:'flex',
    },
    label:{
        textAlign:'center',
        fontSize: 14,
        fontWeight: '400',
        color: '#ffff',
        fontFamily: 'HelveticaNeue',
        padding:20,
        alignSelf:'center',
    },
    btn:{
        marginLeft:30,
        width: '85%',
        backgroundColor: '#7583CA',
        borderRadius: 20,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        borderWidth:1,
        borderColor: '#A1A4B2',
    },
    socialIcon:{
        position:'absolute',
        left:30,
    },
})
export default SecondaryButton;