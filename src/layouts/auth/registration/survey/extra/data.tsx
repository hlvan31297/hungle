import { IMAGE } from '../../../../../components/image';
const lineBreak = `\n`;
export const Title = {
  Title_one: `By sign in up you accept Term of service ${lineBreak}and privacy policy.`,
};
export const defaultOptions_one: SelectOptionType[] = [
  {
    title: `青山学院大学`,
    id: 1,
  },
  {
    title: `学 習院大学`,
    id: 2,
  },
  {
    title: `慶応義塾大学`,
    id: 3,
  },
  {
    title: `國學院大学`,
    id: 4,
  },
  {
    title: `国際木基督教大学`,
    id: 5,
  }
];
export const defaultOptions_two : SelectOptionType[]=[
  {
    title: `テニス部`,
    id: 1,
  },
  {
    title: `サッカー部`,
    id: 2,
  },
  {
    title: `バレー部`,
    id: 3,
  },
  {
    title: `バドミントン部`,
    id: 4,
  },
  {
    title: `ラグビー部`,
    id: 5,
  },

]

export interface SelectOptionType {
  title: string;
  id: number;
  items?: { title: string }[];
};




