import { IMAGE } from '../../../../../components/image';
export const select : SelectOptionType[] = [
    {
      title: `認証コードが届かない`,
      id: 1,
    },
    {
      title: `ログインできない`,
      id: 2,
    },
    {
      title: `投稿について`,
      id: 3,
    },
    {
      title: `プロフィールについて`,
      id: 4,
    },
    {
      title: `国際木基督教大学`,
      id: 5,
    },
    {
        title: `推薦コメントについて`,
        id: 6,
    },
    {
        title: `オファー/チャレンジについて`,
        id: 7,
    },
    {
        title: `公開制限について`,
        id: 8,
    },
    {
        title: `退会について`,
        id: 9,
    },
    {
        title: `その他`,
        id: 10,
    },  
  ];
export interface SelectOptionType {
    title: string;
    id: number;
    items?: { title: string }[];
  };